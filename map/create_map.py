import sys
import os
os.environ['LD_LIBRARY_PATH'] = ':'.join([os.environ.get('LD_LIBRARY_PATH', ''), '/opt/LCIO/lib'])
sys.path.append('/opt/LCIO/src/python')
from pyLCIO import EVENT, UTIL, IOIMPL, IMPL
import numpy as np
#import h5py 
import awkward as ak 
import argparse
import math
from functions import CellIDDecoder
import argparse
import pickle


def find_nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return array[idx], idx

def fill_record(inpLCIO, collection, nevents):
    """this function reads all events in LCIO file and puts them into awkward array"""

    ## open LCIO file
    reader = IOIMPL.LCFactory.getInstance().createLCReader()
    print ("Opening the file: ", inpLCIO)
    reader.open( inpLCIO )

    
    ## create awkward array
    b = ak.ArrayBuilder()

    ## start looping
    nEvt = 0
    for evt in reader:
        nEvt += 1
        if nEvt > nevents:
            break

        
        ## First thing: MC particle collection
        b.begin_list()
        mcparticle = evt.getCollection("MCParticle")    
        
        ## fill energy for each MCParticle (in this case just an incoming photon)
        for enr in mcparticle:
            b.begin_record()
            b.field("E")
            b.real(enr.getEnergy())
            ## calculate polar angle theta and fill
            b.field("theta")
            pVec = enr.getMomentum()
            theta = math.pi/2.00 - math.atan(pVec[2]/pVec[1])
            b.real(theta)
            b.end_record() 
    
        ## ECAL barrel collection
        ecalBarrel = evt.getCollection(collection)
        cellIDString = ecalBarrel.getParameters().getStringVal("CellIDEncoding")
        decoder = CellIDDecoder( cellIDString ) 
        ##

        for hit in ecalBarrel:

            l = decoder.layer( hit.getCellID0() ) ## get the layer information from CellID0 
            e = hit.getEnergy() 
            pos = hit.getPosition()

            ## start filling a record with all relevant information
            b.begin_record() 
            b.field("x")
            b.real(pos[0])
            b.field("y")
            b.real(pos[1])
            b.field("z")
            b.real(pos[2])
            b.field("e")
            b.real(e * 1000)
            b.field("layer")
            b.integer(l)
            b.field('cid0')
            b.real(hit.getCellID0())
            b.field('cid1')
            b.real(hit.getCellID1())
            b.end_record() 

        b.end_list()

    ### Example:
    # Get the incident energy of the first event --> b[0].E
    # Get the x positions of the the first event --> b[0].x 

    return b



def fill_map(record, nEvents):
    """this function reads the awkward array and creates a cell map"""
    
    
    ## these are the layer positions in y[mm] (depth of the calorimeter)
    hmap = np.array([1811, 1814, 1824, 1827, 1836, 1839, 1849,
                    1852, 1861, 1864, 1873, 1877, 1886, 1889, 1898, 1902,
                    1911, 1914, 1923, 1926, 1938, 1943, 1955, 1960,
                    1971, 1976, 1988, 1993, 2005, 2010])


    #defined binning
    binX = np.arange(-81, 82, 5.088333)
    #binZ large 
    binZ = np.arange(-119, 201, 5.088333)

    ## Unable to escape using python list here. But we can live with that.
    cell_map = {
        (0, 0, 0): [0.0, 0.0, 0.0, 0.0, 0.0 ]
    }

    for i in range(0, nEvents):

        #Get hits and convert them into numpy array 
        z = ak.to_numpy(record[i].z)
        x = ak.to_numpy(record[i].x)
        y = ak.to_numpy(record[i].y)
        e = ak.to_numpy(record[i].e)
        cid0 = ak.to_numpy(record[i].cid0)
        cid1 = ak.to_numpy(record[i].cid1)

        #loop over layers and project them into 2d grid.
        for j in range(0,30):
            idx = np.where((y <= (hmap[j] + 0.9999)) & (y > (hmap[j] + 0.0001)))
            xlayer = x.take(idx)[0]
            zlayer = z.take(idx)[0]
            elayer = e.take(idx)[0]
            c0layer = cid0.take(idx)[0]
            c1layer = cid1.take(idx)[0]
            H, xedges, yedges = np.histogram2d(xlayer, zlayer, bins=(binX, binZ), weights=elayer)
            

            non_x, non_z = np.nonzero(H) 
            for k in range(0,len(non_x)):
                xval = xedges[non_x[k]]
                zval = yedges[non_z[k]]
                x_real, idX = find_nearest(xlayer, xval)
                z_real, idZ = find_nearest(zlayer, zval)

                cell_map[(j, non_x[k], non_z[k])] =  [x_real, hmap[j], z_real, c0layer.take(idX), c1layer.take(idZ)]
   
       

    return cell_map

if __name__=="__main__":

    parser = argparse.ArgumentParser()
   
    parser.add_argument('--lcio', type=str, required=True, help='input LCIO file')
    parser.add_argument('--nEvents', type=int, help='number of events', default=100)
    
    opt = parser.parse_args()
    lcioFile = str(opt.lcio)
    nEvents = int(opt.nEvents)
    
    collection = 'EcalBarrelCollection'
    record = fill_record(lcioFile, collection, nEvents)
    mapFile = fill_map(record, nEvents)
    
    pf = open("/eos/user/e/eneren/photonAngleReco/cell_map5k.pickle","wb")
    
    pickle.dump(mapFile,pf)
    pf.close()
    
    ## meta data for pickle
    f = open("/tmp/pickle_path", "w")
    f.write('/eos/user/e/eneren/photonAngleReco/cell_map5k.pickle')
    f.close()

    
    
    
    
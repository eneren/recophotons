import numpy as np
import argparse
import array as arr


import os
import sys
os.environ['LD_LIBRARY_PATH'] = ':'.join([os.environ.get('LD_LIBRARY_PATH', ''), '/opt/LCIO/lib'])
sys.path.append('/opt/LCIO/src/python')
import os 
import pickle
import random
import math



from pyLCIO import EVENT, UTIL, IOIMPL, IMPL

def get_parser():
    parser = argparse.ArgumentParser(
        description='Generation',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument('--pdg', action='store',
                        type=int, default=22,
                        help='Particle Type')

    parser.add_argument('--mass', action='store',
                        type=int, default=0,
                        help='Mass')
    
    parser.add_argument('--charge', action='store',
                        type=int, default=0,
                        help='Charge')

    parser.add_argument('--numpyShowers', action='store',
                        type=str,
                        help='path of numpy showers')
    
    parser.add_argument('--output', action='store',
                        type=str,
                        help='name of generated lcio file')

    parser.add_argument('--angleMin', action='store',
                        type=int, default=30,
                        help='Minimum angle of incident particle [Rad]')
    
    parser.add_argument('--angleMax', action='store',
                        type=int, default=90,
                        help='Maximum angle of incident particle [Rad]')

    parser.add_argument('--Emax', action='store',
                        type=int, default=100,
                        help='Maximum energy of incident particle [GeV]')
    
    parser.add_argument('--Emin', action='store',
                        type=int, default=10,
                        help='Minumum energy of incident particle [GeV]')

    return parser



def write_to_lcio(showers, outfile, nevt, pdg, mass, charge, angleMax, angleMin, Emax, Emin):
    
    
    ## get the dictionary
    f = open('/eos/user/e/eneren/photonAngleReco/cell_map5k.pickle', 'rb')
    cmap = pickle.load(f)  

    wrt = IOIMPL.LCFactory.getInstance().createLCWriter( )
    wrt.open( outfile , EVENT.LCIO.WRITE_NEW ) 

    random.seed()



    #========== MC particle properties ===================
    genstat  = 1
    


    # write a RunHeader
    run = IMPL.LCRunHeaderImpl() 
    run.setRunNumber( 0 ) 
    run.parameters().setValue("Generator", "BIB-AE")
    run.parameters().setValue("PDG", pdg )
    wrt.writeRunHeader( run ) 

    for j in range(0, nevt):

        ### MC particle Collections
        colmc = IMPL.LCCollectionVec( EVENT.LCIO.MCPARTICLE ) 

        ## we are shooting with an angle to ECAL 
        
        phi =  math.pi / 2.          ## 90deg fixed phi
        grd = random.uniform(angleMin,angleMax)  ## theta is uniform 30 --> 90 deg
        theta = grd/180. * math.pi 

        p = random.uniform(Emin,Emax)     ## Energy is uniform: 10 GeV --> 100 GeV
        
        px = p * math.cos( phi ) * math.sin( theta ) 
        py = p * math.sin( phi ) * math.sin( theta )
        pz = p * math.cos( theta ) 

        momentum  = arr.array('f',[ px, py, pz ] )  

        vx = 0.00
        vy = 1810.00
        vz = -50.000

        vertex = arr.array('d',[vx,vy,vz])

        epx = 0.00
        epy = 1810.00
        epz = -50.00 

        endpoint = arr.array('d',[ epx, epy, epz ] )  


        mcp = IMPL.MCParticleImpl() 
        mcp.setGeneratorStatus( genstat ) 
        mcp.setMass( mass )
        mcp.setPDG( pdg ) 
        mcp.setMomentum( momentum )
        mcp.setCharge( charge )
        mcp.setVertex(vertex)
        mcp.setEndpoint(endpoint)

        colmc.addElement( mcp )
        
        evt = IMPL.LCEventImpl() 
        evt.setEventNumber( j ) 
        evt.addCollection( colmc , "MCParticle" )

        ### END OF MC Particle
        
        ### Calorimeter Collections
        col = IMPL.LCCollectionVec( EVENT.LCIO.SIMCALORIMETERHIT ) 
        flag =  IMPL.LCFlagImpl(0) 
        flag.setBit( EVENT.LCIO.CHBIT_LONG )
        flag.setBit( EVENT.LCIO.CHBIT_ID1 )

        col.setFlag( flag.getFlag() )

        col.parameters().setValue(EVENT.LCIO.CellIDEncoding, 'system:0:5,module:5:3,stave:8:4,tower:12:4,layer:16:6,wafer:22:6,slice:28:4,cellX:32:-16,cellY:48:-16')
        evt.addCollection( col , "EcalBarrelCollection" )

        for layer in range(30):              ## loop over layers
            nx, nz = np.nonzero(showers[j][layer])   ## get non-zero energy cells  
            for k in range(0,len(nx)):
                try:
                    cell_energy = showers[j][layer][nx[k]][nz[k]] / 1000.0
                    tmp = cmap[(layer, nx[k], nz[k])]

                    sch = IMPL.SimCalorimeterHitImpl()

                    position = arr.array('f', [tmp[0],tmp[1],tmp[2]])
    
                    sch.setPosition(position)
                    sch.setEnergy(cell_energy)
                    sch.setCellID0(int(tmp[3]))
                    sch.setCellID1(int(tmp[4]))
                    col.addElement( sch )

                except KeyError:
                    # Key is not present
                    pass
                    
                                
       
        
        wrt.writeEvent( evt ) 

    
    print('LCIO file was created: {}'.format(outfile))
    print("We are ready to run reconstruction via iLCsoft")
    wrt.close() 

    
    
if __name__ == "__main__":

    parser = get_parser()
    parse_args = parser.parse_args() 
    
    pdgid = parse_args.pdg
    mass = parse_args.mass
    charge = parse_args.charge
    aMax = parse_args.angleMax
    aMin = parse_args.angleMin
    engrMax = parse_args.Emax
    engrMin = parse_args.Emin
    path_np = parse_args.numpyShowers
    outputFile = parse_args.output
    
    shower_numpy = np.load(path_np)
    nevents = shower_numpy.shape[0]
    outputEOS = '/eos/user/e/eneren/photonAngleReco/Fixed_generated_showers/' 
    
    ## Meta data
    f = open("/tmp/lcio_path", "w")
    f.write(outputEOS + outputFile)
    f.close()

    
    write_to_lcio(shower_numpy, outputEOS + outputFile, nevents, pdgid, mass, charge, aMax, aMin, engrMax,engrMin)
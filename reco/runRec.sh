#!/bin/bash


source /home/ilc/ilcsoft/v02-01-pre/init_ilcsoft.sh
export REC_MODEL=ILD_l5_o1_v02


git clone --branch v02-01-pre02 https://github.com/iLCSoft/ILDConfig.git
cd ./ILDConfig/StandardConfig/production
cp /home/recophotons/reco/SiWEcalDigi.xml ./CaloDigi/SiWEcalDigi.xml

export LCIO=$1
export outputF=$2

echo "-- Running Reconstruction--"


Marlin MarlinStdReco.xml --constant.lcgeo_DIR=$lcgeo_DIR \
        --constant.DetectorModel=${REC_MODEL} \
        --constant.OutputBaseName=$outputF \
        --constant.RunBeamCalReco=false \
        --global.LCIOInputFiles=$LCIO


echo $outputF\_REC.slcio > /tmp/lcio_rec_path

exit 0;

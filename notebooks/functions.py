from ROOT import TH1D, TCanvas, TF1, std
from pyLCIO import EVENT, UTIL, IOIMPL, IMPL
import matplotlib.pyplot as plt
import numpy as np

import math
import sys
import string
from scipy.optimize import curve_fit

def interval_quantile_(x, quant=0.9):
    """Calculate the shortest interval that contains the desired quantile"""
    x = np.sort(x)
    # the number of possible starting points
    n_low = int(len(x) * (1 - quant))
    # the number of events contained in the quantil
    n_quant = len(x) - n_low

    # Calculate all distances in one go
    distances = x[-n_low:] - x[:n_low]
    i_start = np.argmin(distances)

    return i_start, i_start + n_quant

def gauss(x, *p):
    A, mu, sigma = p
    return A*np.exp(-(x-mu)**2/(2.*sigma**2))

def fit90(x):     
    x = np.sort(x)

    n10percent = int(round(len(x)*0.1))
    n90percent = len(x) - n10percent
    
    start, end = interval_quantile_(x, quant=0.9)
    
    rms90 = np.std(x[start:end])
    mean90 = np.mean(x[start:end])
    mean90_err = rms90/np.sqrt(n90percent)
    rms90_err = rms90/np.sqrt(2*n90percent)   # estimator in root
    return mean90, rms90, mean90_err, rms90_err

def find_nearest(array, value):
    idx = (np.abs(array - value)).argmin()
    return array[idx], idx

def fill_record(inpLCIO, collection, nevents):
    """this function reads all events in LCIO file and puts them into awkward array"""

    ## open LCIO file
    reader = IOIMPL.LCFactory.getInstance().createLCReader()
    reader.open( inpLCIO )

    ## create awkward array
    b = ak.ArrayBuilder()

    ## start looping
    nEvt = 0
    for evt in reader:
        nEvt += 1
        if nEvt > nevents:
            break

        
        ## First thing: MC particle collection
        b.begin_list()
        mcparticle = evt.getCollection("MCParticle")    
        
        ## fill energy for each MCParticle (in this case just an incoming photon)
        for enr in mcparticle:
            b.begin_record()
            b.field("E")
            b.real(enr.getEnergy())
            ## calculate polar angle theta and fill
            b.field("theta")
            pVec = enr.getMomentum()
            theta = math.pi/2.00 - math.atan(pVec[2]/pVec[1])
            b.real(theta)
            b.end_record() 
    
        ## ECAL barrel collection
        ecalBarrel = evt.getCollection(collection)
        cellIDString = ecalBarrel.getParameters().getStringVal("CellIDEncoding")
        decoder = CellIDDecoder( cellIDString ) 
        ##

        for hit in ecalBarrel:

            l = decoder.layer( hit.getCellID0() ) ## get the layer information from CellID0 
            e = hit.getEnergy() 
            pos = hit.getPosition()

            ## start filling a record with all relevant information
            b.begin_record() 
            b.field("x")
            b.real(pos[0])
            b.field("y")
            b.real(pos[1])
            b.field("z")
            b.real(pos[2])
            b.field("e")
            b.real(e * 1000)
            b.field("layer")
            b.integer(l)
            b.field('cid0')
            b.real(hit.getCellID0())
            b.field('cid1')
            b.real(hit.getCellID1())
            b.end_record() 

        b.end_list()

    ### Example:
    # Get the incident energy of the first event --> b[0].E
    # Get the x positions of the the first event --> b[0].x 

    return b



def fill_map(record):
    """this function reads the awkward array and creates a cell map"""
    
    
    ## these are the layer positions in y[mm] (depth of the calorimeter)
    hmap = np.array([1811, 1814, 1824, 1827, 1836, 1839, 1849,
                    1852, 1861, 1864, 1873, 1877, 1886, 1889, 1898, 1902,
                    1911, 1914, 1923, 1926, 1938, 1943, 1955, 1960,
                    1971, 1976, 1988, 1993, 2005, 2010])


    #defined binning
    binX = np.arange(-81, 82, 5.088333)
    #binZ large 
    binZ = np.arange(-119, 201, 5.088333)

    ## Unable to escape using python list here. But we can live with that.
    cell_map = {
        (0, 0, 0): [0.0, 0.0, 0.0, 0.0, 0.0 ]
    }

    for i in range(0, nevents):

        #Get hits and convert them into numpy array 
        z = ak.to_numpy(record[i].z)
        x = ak.to_numpy(record[i].x)
        y = ak.to_numpy(record[i].y)
        e = ak.to_numpy(record[i].e)
        cid0 = ak.to_numpy(record[i].cid0)
        cid1 = ak.to_numpy(record[i].cid1)

        #loop over layers and project them into 2d grid.
        for j in range(0,30):
            idx = np.where((y <= (hmap[j] + 0.9999)) & (y > (hmap[j] + 0.0001)))
            xlayer = x.take(idx)[0]
            zlayer = z.take(idx)[0]
            elayer = e.take(idx)[0]
            c0layer = cid0.take(idx)[0]
            c1layer = cid1.take(idx)[0]
            H, xedges, yedges = np.histogram2d(xlayer, zlayer, bins=(binX, binZ), weights=elayer)
            

            non_x, non_z = np.nonzero(H) 
            for k in range(0,len(non_x)):
                xval = xedges[non_x[k]]
                zval = yedges[non_z[k]]
                x_real, idX = find_nearest(xlayer, xval)
                z_real, idZ = find_nearest(zlayer, zval)

                cell_map[(j, non_x[k], non_z[k])] =  [x_real, hmap[j], z_real, c0layer.take(idX), c1layer.take(idZ)]
   
       

    return cell_map

def plot_esum(real, fake, nbins, minE, maxE, ymax, name, xtitle, typ, fit_stat=False, p0 = [150., 90, 3.]):
    
    
    figSE = plt.figure(figsize=(6,6*0.77/0.67))
    axSE = figSE.add_subplot(1,1,1)


    pSEa = axSE.hist(real, bins=nbins, 
            histtype='stepfilled', color='lightgrey',
            range=[minE, maxE])
    pSEb = axSE.hist(fake, bins=nbins, 
            histtype='step', color='red',
             range=[minE, maxE])

    
    
    axSE.text(0.60, 0.87, 'GEANT 4', horizontalalignment='left',verticalalignment='top', 
             transform=axSE.transAxes, color = 'grey')
    
    axSE.text(0.60, 0.78, 'BIB-AE', horizontalalignment='left',verticalalignment='top', 
             transform=axSE.transAxes, color = 'red')
       
    axSE.text(0.6,
            0.95,
            typ, horizontalalignment='left',verticalalignment='top', 
             transform=axSE.transAxes)

    
    if fit_stat:
        # p0 is the initial guess for the fitting coefficients (A, mu and sigma above)\n",
        

        bin_centres = (pSEa[1][:-1] + pSEa[1][1:]) / 2 
        bin_centres_wgan = (pSEb[1][:-1] + pSEb[1][1:]) / 2 

        coeff, var_matrix = curve_fit(gauss, bin_centres, pSEa[0],p0=p0)
        coeff_wgan, var_matrix_wgan = curve_fit(gauss, bin_centres_wgan, pSEb[0],p0=p0)
        hist_fit = gauss(bin_centres, *coeff)
        hist_fit_wgan = gauss(bin_centres_wgan, *coeff_wgan)
        
        print("REAL: ", coeff[1], ' + ', coeff[2])
        print("FAKE: ", coeff_wgan[1], ' + ', coeff_wgan[2])
        
        axSE.text(0.6, 0.83, "$\sigma$ / $\mu$ = {:.4f}".format(coeff[2] / coeff[1]), horizontalalignment='left',verticalalignment='top', 
                     transform=axSE.transAxes, color = 'grey', fontsize=12)

    
        axSE.text(0.6, 0.74, "$\sigma$ / $\mu$ = {:.4f}".format(coeff_wgan[2] / coeff_wgan[1] ), horizontalalignment='left',verticalalignment='top', 
                     transform=axSE.transAxes, color = 'red', fontsize=12)

    figSE.patch.set_facecolor('white') 
        
    axSE.set_xlim([minE, maxE])
    axSE.set_ylim([0, ymax])
    axSE.set_xlabel(xtitle, family='serif')
    
    plt.savefig('./esum'+str(name)+'.png')

    

def fracEsum(fName, maxEvt, collection):
    reader = IOIMPL.LCFactory.getInstance().createLCReader()
    reader.open( fName )
    
    esum20l = []
    esum10l = []

    
    nEvt = 0

    for evt in reader:
        nEvt += 1
        if nEvt > maxEvt:
                break
        ecalBarrel = evt.getCollection(collection)
        cellIDString = ecalBarrel.getParameters().getStringVal("CellIDEncoding")
        decoder = CellIDDecoder( cellIDString ) 
        esum20 = 0.0
        esum10 = 0.0 
        for hit in ecalBarrel:
            l = decoder.layer( hit.getCellID0() ) 
            e = hit.getEnergy() 
            #print ("Energy:", hit.getEnergy(), " Cell ID0:", hit.getCellID0(), " layer: ", decoder.layer( hit.getCellID0() )) 
            if l < 20:
                esum20 += e 
            elif l >= 20:
                esum10 = esum10 + e*2
        
        esum20l.append(esum20)
        esum10l.append(esum10)
        
    esum20np = np.asarray(esum20l)
    esum10np = np.asarray(esum10l)
    
    return esum20np, esum10np

def plt_NPFOs(data_real, data_fake, energy_center, maxN, minN, bins, xtitle,save_title):
    figSE = plt.figure(figsize=(8,8))
    axSE = figSE.add_subplot(1,1,1)
    lightblue = (0.1, 0.1, 0.9, 0.3)
    
   
    pSEa = axSE.hist(data_real, bins=bins, range=[minN, maxN], density=None, edgecolor='grey', 
                   label = "orig", linewidth=1, color = 'lightgrey',
                   histtype='stepfilled')

    
    pSpnEb = axSE.hist(data_fake, bins=pSEa[1], range=None, density=None, edgecolor='red',
                   label = "orig", linewidth=1,
                   histtype='step')
   
    axSE.set_xlabel(xtitle, family='serif')
    axSE.set_xlim([minN, maxN])
    axSE.set_ylim([0, 1800])
    posX= 0.02
    
    axSE.text(posX + 0.6, 0.65, "Geant4", horizontalalignment='left',verticalalignment='top', 
             transform=axSE.transAxes, color = 'grey')
    
    axSE.text(posX + 0.6, 0.55, "BiB-AE", horizontalalignment='left',verticalalignment='top', 
             transform=axSE.transAxes, color = 'red')
    
    axSE.text(posX,
            0.97,
            '{:d} GeV'.format(energy_center), horizontalalignment='left',verticalalignment='top', 
             transform=axSE.transAxes)

   
    plt.subplots_adjust(left=0.18, right=0.95, top=0.95, bottom=0.18)
   
    figSE.patch.set_facecolor('white')

    #hep.cms.label(loc=0)
    plt.savefig(save_title+ str(energy_center)+"_single_NPFOs_comp.png")

def pfo_energy(fName, maxEvt, collection, npfoCut=5):

    reader = IOIMPL.LCFactory.getInstance().createLCReader()
   
    reader.open( fName )

    nEvt = 0
    esuml = []
    nPFOs = []
  
    
    for evt in reader:
        nEvt += 1
        #print('Start event... ')
        if nEvt > maxEvt:
                break
        col = evt.getCollection(collection)
        nPFOc = 0
        if len(col) > npfoCut: continue
        
        for c in col:
            e = c.getEnergy()  ## get energy
            esuml.append(e)
            nPFOc += 1
            
        nPFOs.append(nPFOc)
        
    esumnp = np.asarray(esuml)
    nPFOs_np = np.asarray(nPFOs)
        
    return esumnp, nPFOs_np


## returns fractional energy: first 20 layers and last 10 layers
def fracEsum(fName, maxEvt, collection):
    reader = IOIMPL.LCFactory.getInstance().createLCReader()
    reader.open( fName )
    
    esum20l = []
    esum10l = []

    
    nEvt = 0

    for evt in reader:
        nEvt += 1
        if nEvt > maxEvt:
                break
        ecalBarrel = evt.getCollection(collection)
        cellIDString = ecalBarrel.getParameters().getStringVal("CellIDEncoding")
        decoder = CellIDDecoder( cellIDString ) 
        esum20 = 0.0
        esum10 = 0.0 
        for hit in ecalBarrel:
            l = decoder.layer( hit.getCellID0() ) 
            e = hit.getEnergy() 
            #print ("Energy:", hit.getEnergy(), " Cell ID0:", hit.getCellID0(), " layer: ", decoder.layer( hit.getCellID0() )) 
            if l < 20:
                esum20 += e 
            elif l >= 20:
                esum10 = esum10 + e*2
        
        esum20l.append(esum20)
        esum10l.append(esum10)
        
    esum20np = np.asarray(esum20l)
    esum10np = np.asarray(esum10l)
    
    return esum20np, esum10np
    


def Esumhit(fName, maxEvt, collection):
    reader = IOIMPL.LCFactory.getInstance().createLCReader()
    reader.open( fName )
    
    esuml = []

    nEvt = 0

    for evt in reader:
        nEvt += 1
        if nEvt > maxEvt:
                break
        ecalBarrel = evt.getCollection(collection)
    
        cellIDString = ecalBarrel.getParameters().getStringVal("CellIDEncoding")
        decoder = CellIDDecoder( cellIDString ) 
        esum = 0.0
        for hit in ecalBarrel:
            l = decoder.layer( hit.getCellID0() ) 
            e = hit.getEnergy() 
            esum += e
        
        esuml.append(esum)
        
        
    esumnp = np.asarray(esuml)
    
    
    return esumnp

def nhits(fName, maxEvt, collection):
    reader = IOIMPL.LCFactory.getInstance().createLCReader()
    reader.open( fName )
    
    esuml = []

    nEvt = 0
    nhits = []
    for evt in reader:
        nEvt += 1
        if nEvt > maxEvt:
                break
        col = evt.getCollection(collection)
        
        for c in col:
            hits = c.getCalorimeterHits().size()
        nhits.append(hits)    
     
    nphits = np.asarray(nhits)
    return nphits

def nhits_sim(fName, maxEvt, collection):
    reader = IOIMPL.LCFactory.getInstance().createLCReader()
    reader.open( fName )
    
    nhits = []

    nEvt = 0
    #cut = 0.1e-03
    cut = 0.0
    for evt in reader:
        nEvt += 1
        if nEvt > maxEvt:
                break
        ecalBarrel = evt.getCollection(collection)
    
        cellIDString = ecalBarrel.getParameters().getStringVal("CellIDEncoding")
        decoder = CellIDDecoder( cellIDString ) 
        hits = 0
        for hit in ecalBarrel:
            if (hit.getEnergy() > cut):  
                hits += 1 
         
        
        nhits.append(hits)
        
        
    nsumnp = np.asarray(nhits)
    
    
    return nsumnp



class CellIDDecoder:

    """ decoder for LCIO cellIDs """

    def __init__(self,encStr):
        self.encStr=encStr
        self.funs = {} 

        tokens = encStr.split(',')
        
        offset = 0
        
        for t in tokens:
        
         # print "token: " , t
        
          st = t.split(':')
        
          if len(st)==2:
            name = st[0]
            start = offset 
            width = int(st[1])
            offset += abs( width )
        
          elif len(st)==3:
            name = st[0]
            start = int(st[1]) 
            width = int(st[2])
            offset = start + abs( width )
        
        
          else:
            print ("unknown token:" , t)
        
          mask = int(0x0)
          for i in range(0,abs(width)):
            mask = mask | ( 0x1 << ( i + start) )
        
          setattr( CellIDDecoder , name , self.makefun( mask, start , width) )


    def makefun(self, mask,start,width):
      if( width > 0 ):
        return ( lambda ignore, cellID : (( mask & cellID) >> start )  )
      else:
        return ( lambda ignore, cellID : (~(( mask & cellID) >> start )  ^ 0xffffffff) )
